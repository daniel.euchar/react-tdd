import React from 'react';
import Message from '../Message';
import {create} from 'react-test-renderer';

test('should contain Textarea inside component', () => {
    const message = create(<Message />).root;
    const element = message.findByType("textarea");
    expect(element.type).toBe('textarea');
})

test('Textarea should contain placeholder', () => {
    const message = create(<Message />).root;
    const element = message.findByType("textarea");
    expect(element.props.placeholder).toBe('Enter Your Feedback');
})

test('Textarea should contain class form-control', () => {
    const message = create(<Message />).root;
    const element = message.findByType("textarea");
    expect(element.props.className).toBe('form-control');
})
