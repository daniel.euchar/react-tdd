import React, { Component } from 'react'
class Message extends Component  {
    render() {
        return (
            <div className="offset-md-2 col-md-8" style={{padding: "10px"}}>
                <textarea name="message" className="form-control" rows="5" placeholder="Enter Your Feedback"></textarea>
            </div>
        )
    }
}

export default Message;
