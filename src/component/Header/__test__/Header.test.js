import React from 'react';
import {create } from 'react-test-renderer';
import Header from '../header';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'

test("Header component should contain h1 tag",()=>{
    const header = create(<Header />).root;
    const element = header.findByType("h1");
    expect(element.type).toBe('h1');
});

it("renders header correctly", () => {
  const {getByTestId} = render(<Header/>);
  const message = 'Feedback Form';
  expect(getByTestId("header")).toHaveTextContent(message);
})
