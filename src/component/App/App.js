import React from 'react';
import Form from '../Form/Form'
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component{
  render(){
    return (
      <div className="App container">
        <Form />
      </div>
    );
  }
}

export default App;
