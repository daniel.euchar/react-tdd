import React,{Component} from 'react';

class Type extends Component{
  render(){
    return(
      <div>
        <div className="form-check form-check-inline">
          <input type="radio" className="form-check-input" id="comments" name="type" value="Comments" />
          <label className="form-check-label" htmlFor="materialUnchecked">Comments</label>
        </div>
        <div className="form-check form-check-inline">
          <input type="radio" className="form-check-input" id="bugreport" name="type" value="Bug Report" />
          <label className="form-check-label" htmlFor="materialUnchecked">Bug Report</label>
        </div>
        <div className="form-check form-check-inline">
          <input type="radio" className="form-check-input" id="questions" name="type" value="Question" />
          <label className="form-check-label" htmlFor="materialUnchecked">Question</label>
        </div>
      </div>

    )
  }
}

export default Type;
