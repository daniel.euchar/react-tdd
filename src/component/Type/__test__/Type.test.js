import React from 'react';
import Type from '../Type';
import {create} from 'react-test-renderer';

test('should contain radio inputs', () => {
    const type = create(<Type />).root;
    const element = type.findAllByType("input");
    element.forEach((ele) => {
      expect(ele.props.type).toBe('radio');
    })
})

test('should contain 3 radio inputs', () => {
    const type = create(<Type />).root;
    const element = type.findAllByType("input");
    expect(element.length).toBe(3);
})
