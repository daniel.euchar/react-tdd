import React from 'react';
import UserDetails from '../UserDetails';
import {create} from 'react-test-renderer';

test('should contain inputs for name & email', () => {
    const type = create(<UserDetails />).root;
    const element = type.findAllByType("input");
    expect(element[0].props.name).toBe('name');
    expect(element[1].props.name).toBe('email');
})

test('should contain 2 inputs', () => {
    const type = create(<UserDetails />).root;
    const element = type.findAllByType("input");
    expect(element.length).toBe(2);
})
