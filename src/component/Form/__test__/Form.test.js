import React from 'react';
import Header from '../../Header/Header';
import Type from '../../Type/Type';
import UserDetails from '../../UserDetails/UserDetails';
import Message from '../../Message/Message'
import {create} from "react-test-renderer";
import Form from '../../Form/Form';
import {render, fireEvent} from '@testing-library/react';

test("Should contain a form tag",()=>{
    const app = create(<Form />).root;
    const element = app.findByType("form");
    expect(element.type).toBe("form");
  });
  
  test("Should check if header component exists",()=>{
    const header = create(<Header />);
    expect(header.toJSON()).toMatchSnapshot();
  });
  
  test("Should check message component exists",()=>{
    const message = create(<Message />);
    expect(message.toJSON()).toMatchSnapshot();
  });
  
  test("Should check type component exists",()=>{
    const type = create(<Type />);
    expect(type.toJSON()).toMatchSnapshot();
  });
  
  test("Should check UserDetails component exists",()=>{
    const userDetails = create(<UserDetails/>);
    expect(userDetails.toJSON()).toMatchSnapshot();
  });


test('should trigger the onSubmitHandler twice', () => {
    const onSubmitHandler =jest.fn();
    const {getByTestId} = render(<Form onSubmit={onSubmitHandler}/>);
    const element = getByTestId('form'); 
   
    fireEvent.submit(element);
    expect(onSubmitHandler).toHaveBeenCalledTimes(1);
    //TODO: assert state to be changed


  })
  

  test('should trigger the onSubmitHandler on form submit', () => {
    const {getByTestId} = render(<Form />);
    const element = getByTestId('form'); 
    fireEvent.submit(element);
    const result = getByTestId('result');
    expect(result.innerHTML).toBe('Result');
})