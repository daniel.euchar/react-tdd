import React, { Component } from 'react'
import Header from '../Header/Header';
import Message from '../Message/Message';
import Type from '../Type/Type'
import UserDetails from '../UserDetails/UserDetails';


class Form extends Component{
    onSubmitHandler(){
        document.getElementById("result").innerHTML='Result';
    }
    render() {
        return (
            <>
            <form id="feedback-form" data-testid="form" onSubmit={this.props.onSubmit?this.props.onSubmit:this.onSubmitHandler} method="post">
                <Header />
                <Type />
                <Message />
                <UserDetails />
                <button type="submit" id='submit-button' className="btn btn-primary">Submit</button>
            </form>
            <div data-testid="result" id='result'></div>
            </>
        )
    }
}
export default Form;